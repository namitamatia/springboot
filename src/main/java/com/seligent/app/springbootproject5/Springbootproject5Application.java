package com.seligent.app.springbootproject5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootproject5Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootproject5Application.class, args);
	}
}
