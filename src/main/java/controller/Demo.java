package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Demo {

    @GetMapping("/hello")
    public  String getHelloMessage() {
        return  "Hello Spring boot";
    }

}
